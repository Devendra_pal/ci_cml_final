from asyncio.log import logger
# from ensurepip import version
from importlib.resources import path
import pandas as pd
import numpy as np 
import xgboost
from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score
from sklearn.model_selection import train_test_split
from sklearn.linear_model import ElasticNet
from urllib.parse import urlparse
import mlflow
import mlflow.sklearn
import logging 
from math import sqrt
from sklearn.preprocessing import RobustScaler
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestRegressor,GradientBoostingRegressor
import matplotlib.pyplot as plt
import pickle
logging.basicConfig(level=logging.WARN)
logger =  logging.getLogger(__name__)

import dvc.api

path = 'data//Phase3.csv'
repo = 'https://gitlab.com/Devendra_pal/ci_cml_final//' 
version = 'v3'

data_url = dvc.api.get_url(
    path=path,
    repo= repo,
    rev= version  
)
mlflow.set_experiment('demo')

def eval_metrics(actual, pred):
    rmse = np.sqrt(mean_squared_error(actual,pred))
    mae= mean_absolute_error(actual,pred)
    r2 = r2_score(actual,pred)
    return rmse, mae, r2

if __name__ == "__main__":
    
    

    df_phase4 = pd.read_csv(data_url, sep=",")
    # v1
    # df = df_phase4.drop([ 'SAV.dosed.actual', 'ADA.dosed.actual',
    #    'NaOH.dosed.vol.planned', 'SAV.dosed.planned', 'ADA.dosed.planned','DM115Level','NaOH.dosed.vol.actual', 'PC101Current', 'sulphateProd',
    #       'timestamp','Phase','Unnamed: 0','By_pass_valves' ,'DM05level', 'PC101DischargePress', 'FlowProxy','LP_Steam' ,'FeedgasTemp'],axis=1)
    # v2
    df = df_phase4.drop([ 'SAV.dosed.actual', 'ADA.dosed.actual',
        'DM115Level','NaOH.dosed.vol.actual', 'PC101Current', 'sulphateProd',
          'timestamp','Phase','Unnamed: 0','By_pass_valves' ,'DM05level', 'PC101DischargePress', 'FlowProxy','LP_Steam' ,'FeedgasTemp'],axis=1)
    df.set_index('DateTime', inplace = True)  
    df = df[:-4] 
    df["NaSCN_1D"] = df.NaSCN.shift(-1) 
    training_set_1d = df.drop(['NaSCN_1D'],axis=1)
    t = training_set_1d.shape
    print(t)
    Y_NaSCN_1D= df['NaSCN_1D'] 
    date = df.index[500:530]
    sc = RobustScaler()
    training_set_scaled = sc.fit_transform(training_set_1d)

    predicted_y = []
    for i in range(30):
        X_train_scaled = training_set_scaled[:500+i]
        X_test_scaled  = training_set_scaled[500+i:501+i]
        y_train = Y_NaSCN_1D[:500+i]
        y_test = Y_NaSCN_1D[500+i]
        # reg = xgboost.XGBRegressor(objective='reg:squarederror', \
        #                         n_estimators=1000, \
        #                         nthread=24)
        reg = RandomForestRegressor(n_estimators = 100) 
        # reg = LinearRegression()
        reg.fit(X_train_scaled, y_train)
        predictions_xgb = reg.predict(X_test_scaled)
        predicted_y.append(predictions_xgb) 
    filename = 'model_rf_v1.pkl'
    pickle.dump(reg, open(filename, 'wb'))
    ten_day_og = Y_NaSCN_1D[500:530]
    ten_day_pred = pd.Series(predicted_y) 
    # rmse_xgb = sqrt(mean_squared_error(ten_day_og,ten_day_pred))
    # mae_xgb = mean_absolute_error(ten_day_og,ten_day_pred)
    rmse_rf = sqrt(mean_squared_error(ten_day_og,ten_day_pred))
    mae_rf = mean_absolute_error(ten_day_og,ten_day_pred)
    r2_score1 = r2_score(ten_day_og,ten_day_pred)
    print(rmse_rf , mae_rf, r2_score1 )
    mlflow.log_param("data_url", data_url)
    mlflow.log_param('Model_Name', 'linear')
    mlflow.log_param('data_version',version)
    mlflow.log_param('input_row', t[0])
    mlflow.log_param('input_cols', t[1])
    mlflow.log_metric("RMSE",rmse_rf)
    mlflow.log_metric("MAE",mae_rf)
    mlflow.log_metric("R2",r2_score1)
    mlflow.log_artifact('model_rf_v1.pkl')
    plt.figure(figsize=(20,12)) 
    plt.xticks(rotation=90)
    plt.plot(df.index[500:530],ten_day_pred,color ='tab:blue')
    plt.plot(df.index[500:530],ten_day_og,color ='tab:orange')
    plt.legend(["predicted", "original"],prop={'size': 30})
    plt.savefig("results1.png")
    mlflow.log_artifact("results1.png")




